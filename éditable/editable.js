
let data;
let tableElement = document.createElement('table');

//============ F E T C H ===================
//1 fonction asynchrone

//2 récupère le fichier json, ensuite ça le convertit en JS
// on ajout ".json" pour dire que c'est un fichier JSON qu'on convertit en JS

//3 On affiche notre tableau en HTML grace a notre fonction

//4 log le fichier, noté then car fetch est une fonction asynchrone
// elle agit en paralèle du reste du code
//3
//.then(reponse => console.log(reponse))//4
// ===========================================

// function qui affiche notre tableau
function createTable(elem) {
    // récupérer l'url JSON dans l'attribu HTML
    let jsonURL = elem.getAttribute('data-source');

    // aller chercher les données avec l'API FETCH
    fetch(jsonURL)
        .then((response) => response.json())
        .then(
            (lesdatadufichier) => {
                data = lesdatadufichier
                tableHeader()
                tableBody()
            }
        );
    elem.appendChild(tableElement)

}

// function qui créer le header du tableau
function tableHeader() {
    // prendre un élément du tableau
    let unElement = data[0];
    // créer une ligne pour l'entête du tableau
    let tr = document.createElement('tr');
    // parcourir les clefs sur cet élément
    for (let key in unElement) {
        // pour chaque clef, créer une cellule dans la ligne et mettre la clef dedans
        let th = document.createElement('th');
        th.innerText = key;
        tr.appendChild(th)
    }
    let thead = document.createElement('thead');
    thead.classList.add('tbl-header')
    thead.appendChild(tr)
    tableElement.appendChild(thead)

    // function qui trie les element str et transforme les boolean en str
    function triedString(zob) {
        data = data.sort((a, b) => {
            return (a[zob]).toString().localeCompare(b[zob]).toString();
        })
    }

    function tried(zob) {
        data = data.sort((a, b) => {
            return a[zob] - b[zob];
        });
        tableBody()
    }

    function triedReverse(zob) {
        data = data.sort().reverse((a, b) => {
            return a[zob] - b[zob];
        })
        tableBody()
    }

    let compt = 0

    thead.addEventListener('click', e => {
        if (compt % 2 === 0) {
            tried(e.target.innerHTML)
            compt++
        } else {
            triedReverse(e.target.innerHTML)
            compt++
        }
    }); // event trie et trieReverse

    thead.addEventListener('click', e => {
        triedString(e.target.innerHTML)
    })
    tableBody()
}


// function qui créer le body du tableau
function tableBody() {
    // parcourir le tableau en créant une ligne pour chaque clef
    let tbody = document.createElement('tbody')
    for (let obj of data) {
        let tr = document.createElement('tr');

        // pour chaque valeurs, créer une cellule 
        for (let key in obj) {
            let td = document.createElement('td');
            td.DOCUMENT_FRAGMENT_NODE
            td.innerText = obj[key]
            td.contentEditable = true;
            tr.appendChild(td);

        } tbody.appendChild(tr);
    }
    tableElement.querySelector('tbody')?.remove()
    tableElement.appendChild(tbody)
}

// addEventListener 

// function filter

const filter = document.querySelector("#idfilter");


filter.addEventListener('input', e =>{
    let trs = document.querySelectorAll('tr');// recup tous les tr
    for (let tr of trs){ // for each "of" pour parcourir les valeurs
        if (!tr.innerText.includes(filter.value)){ // si la valeurs dans le input n'est pas includes
            tr.style.display = "none" // ne pas afficher les tr
        }else{ //sinon
            tr.style.display = "" // les afficher
        }
    }
})

//function pour édité le tableau

let cells = document.querySelectorAll("td");
for (let cell of cells){
    cell.addEventListener('input', 'edit');
}

function edit(event){
    let cell = event.target;
    let 
}









